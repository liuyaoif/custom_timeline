﻿using UnityEngine;
using UnityEngine.Playables;

namespace Solution2
{
    [System.Serializable]
    public class ColorTLAsset : PlayableAsset
    {
        public Color Param;

        public override Playable CreatePlayable(PlayableGraph graph, GameObject go)
        {
            return Playable.Create(graph);
        }
    }
}