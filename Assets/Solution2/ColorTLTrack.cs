﻿using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using UnityEngine.UI;

namespace Solution2
{
    [TrackClipType(typeof(ColorTLAsset))]
    [TrackBindingType(typeof(Text))]
    public class ColorTLTrack : TrackAsset
    {
        public override Playable CreateTrackMixer(PlayableGraph graph, GameObject go, int inputCount)
        {
            PlayableDirector direct = graph.GetResolver() as PlayableDirector;

            ScriptPlayable<ColorTLBehaviour> TrackBehaviourPlayable = ScriptPlayable<ColorTLBehaviour>.Create(graph);
            TrackBehaviourPlayable.SetInputCount(inputCount);
            ColorTLBehaviour behaviour = TrackBehaviourPlayable.GetBehaviour();
            behaviour.Owner = this;

            return TrackBehaviourPlayable;
        }
    }
}