﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(CutScenePlayer))]
[CanEditMultipleObjects]
public class CutScenePlayerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        if (GUILayout.Button("构造bindings"))
        {
            CutScenePlayer player = (CutScenePlayer)serializedObject.targetObject;
            player.BindingList = new List<TimelineBindingCfg>();
            foreach (var itr in player.Director.playableAsset.outputs)
            {
                TimelineBindingCfg config = new TimelineBindingCfg();
                config.binding = itr.streamName;
                player.BindingList.Add(config);
            }
        }
    }
}