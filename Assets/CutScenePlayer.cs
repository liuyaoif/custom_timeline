﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

[Serializable]
public struct TimelineBindingCfg
{
    public string binding;
    public UnityEngine.Object target;
}

public class CutScenePlayer : MonoBehaviour
{
    public PlayableDirector Director;
    public List<TimelineBindingCfg> BindingList;

    private void OnGUI()
    {
        if (GUI.Button(new Rect(100, 100, 150, 50), "Play"))
        {
            Prepare();
            PlayDirector();
        }
    }

    private void Prepare()
    {
        int index = 0;
        foreach (PlayableBinding pb in Director.playableAsset.outputs)
        {
            Director.SetGenericBinding(pb.sourceObject, BindingList[index].target);
            index++;
        }
    }

    private void PlayDirector()
    {
        Director.Play();
    }
}