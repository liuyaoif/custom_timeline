﻿using System;
using UnityEngine.Playables;
using UnityEngine.UI;

namespace Solution1
{
    [Serializable]
    // A behaviour that is attached to a playable
    public class TextTLBehaviour : PlayableBehaviour
    {
        public TextTLAsset Owner;
        public string Param;

        private Text Target;

        public override void OnGraphStart(Playable playable)
        {
            Target = (playable.GetGraph().GetResolver() as PlayableDirector).GetGenericBinding(Owner.Owner) as Text;
        }

        public override void OnGraphStop(Playable playable)
        {
        }

        public override void OnBehaviourPlay(Playable playable, FrameData info)
        {
            if (Target != null)
            {
                Target.text = Param;
            }
        }

        public override void OnBehaviourDelay(Playable playable, FrameData info)
        {
        }

        public override void OnBehaviourPause(Playable playable, FrameData info)
        {
            if (Target != null)
            {
                Target.text = string.Empty;
            }
        }

        public override void OnPlayableCreate(Playable playable)
        {
        }

        public override void OnPlayableDestroy(Playable playable)
        {
        }

        public override void PrepareData(Playable playable, FrameData info)
        {
        }

        public override void PrepareFrame(Playable playable, FrameData info)
        {
        }

        public override void ProcessFrame(Playable playable, FrameData info, object playerData)
        {
        }
    }
}