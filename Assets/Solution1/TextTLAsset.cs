﻿using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.UI;

namespace Solution1
{
    [System.Serializable]
    public class TextTLAsset : PlayableAsset
    {
        public TextTLTrack Owner;
        public string Param;

        /// <summary>
        /// 在创建时向Behaviour传引用
        /// </summary>
        /// <param name="graph"></param>
        /// <param name="go"></param>
        /// <returns></returns>
        public override Playable CreatePlayable(PlayableGraph graph, GameObject go)
        {
            var playable = ScriptPlayable<TextTLBehaviour>.Create(graph);

            var behaviour = playable.GetBehaviour();
            behaviour.Owner = this;
            //behaviour.Target = (graph.GetResolver() as PlayableDirector).GetGenericBinding(Owner) as Text;
            behaviour.Param = Param;

            return playable;
        }
    }
}