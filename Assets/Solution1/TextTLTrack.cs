﻿using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using UnityEngine.UI;

namespace Solution1
{
    /*
    解决方案1:
    1. Custom的Track, Asset, Behaviour
    2. 每个Asset创建一个Behaviour, 各自传值传引用
    3. 在Behaviour的OnBehaviourPlay和OnBehaviourPause处理逻辑
    */

    [TrackClipType(typeof(TextTLAsset))]
    [TrackBindingType(typeof(Text))]
    public class TextTLTrack : TrackAsset
    {
        /// <summary>
        /// TrackAsset在创建时必然会遍历所有的Clip,有机会给所有的clip传值
        /// </summary>
        /// <param name="graph"></param>
        /// <param name="gameObject"></param>
        /// <param name="clip"></param>
        /// <returns></returns>
        protected override Playable CreatePlayable(PlayableGraph graph, GameObject gameObject, TimelineClip clip)
        {
            PlayableDirector direct = graph.GetResolver() as PlayableDirector;
            TextTLAsset asset = clip.asset as TextTLAsset;
            asset.Owner = this;

            return base.CreatePlayable(graph, gameObject, clip);
        }
    }
}